#!/bin/bash

# April 2016 Borja Aparicio
# Receives:
# Environment variables
#   EOS_ACCOUNT_USERNAME
#   EOS_ACCOUNT_PASSWORD
#   CI_OUTPUT_DIR => default: public/
#   EOS_PATH
#   EOS_MGM_URL => default: root://eosuser.cern.ch
#   METHOD => default: xrdcp

#
#
# Produces:
#  Uploads to $EOS_PATH in the EOS namespace the files found in CI_WEBSITE_DIR

# Exit if anything fails
set -e

# Authenticate user via Kerberos
kinit="/usr/bin/kinit"
if [ ! -x $kinit ]
then
        echo "ERROR: $kinit not found"
        exit 1
fi

kdestroy="/usr/bin/kdestroy"
if [ ! -x $kdestroy ]
then
        echo "ERROR: $kdestroy not found"
        exit 1
fi

# Validate input
: "${EOS_ACCOUNT_USERNAME:?EOS_ACCOUNT_USERNAME not provided}"
: "${EOS_ACCOUNT_PASSWORD:?EOS_ACCOUNT_PASSWORD not provided}"
: "${EOS_PATH:?EOS_PATH not provided}"

# Directory where the web site has been generated in the CI environment
# If not provided by the user
if [ -z "$CI_OUTPUT_DIR" ];
then
	CI_OUTPUT_DIR="public/"
fi

# Check the source directory exists
if [ ! -d "$CI_OUTPUT_DIR" ]
then
	echo "ERROR: Source directory '$CI_OUTPUT_DIR' doesn't exist"
	exit 1
fi

# EOS MGM URL, if not provided by the user
if [ -z "$EOS_MGM_URL" ];
then
	EOS_MGM_URL="root://eosuser.cern.ch"
fi

# Get credentials
echo "$EOS_ACCOUNT_PASSWORD" | $kinit "$EOS_ACCOUNT_USERNAME@CERN.CH" 2>&1 >/dev/null
if [ $? -ne 0 ]
then
	echo "Failed to get Krb5 credentials for '$EOS_ACCOUNT_USERNAME'"
        exit 1
fi

case "$METHOD" in
    'rsync')
        echo "Using rsync"
        source deploy-eos-rsync.sh
        ;;
    *)
        echo "Using xrdcp"
        source deploy-eos-xrdcp.sh
        ;;
esac

# Destroy credentials
$kdestroy
if [ $? -ne 0 ]
then
    echo "Krb5 credentials for '$EOS_ACCOUNT_USERNAME' have not been cleared up"
fi

echo "Updated EOS web site in '$EOS_PATH'"
exit 0
